
# <img src='img/hex-logo.png' align="right" height="200" /> behaviorchange 📦

behaviorchange: Tools for Behavior Change Researchers and Professionals

<!-- badges: start -->

[![Pipeline
status](https://gitlab.com/r-packages/behaviorchange/badges/master/pipeline.svg)](https://gitlab.com/r-packages/behaviorchange/commits/master)

[![Coverage
status](https://codecov.io/gl/r-packages/behaviorchange/branch/master/graph/badge.svg)](https://codecov.io/gl/r-packages/behaviorchange?branch=master)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://r-packages.gitlab.io/behaviorchange>.

<!--------------------------------------------->

<!-- Start of a custom bit for every package -->

<!--------------------------------------------->

## jamovi

This package is also available as a module in jamovi, which is freely
available at <https://jamovi.org>.

## The Book of Behavior Change

The package authors are working on an open access book on behavior
change. The in progress version is already available from
<https://bookofbehaviorchange.com>.

<!--------------------------------------------->

<!--  End of a custom bit for every package  -->

<!--------------------------------------------->

## Installation

You can install the released version of `behaviorchange` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('behaviorchange');
```

You can install the development version of `behaviorchange` from
[GitLab](https://gitlab.com) with:

``` r
devtools::install_gitlab('r-packages/behaviorchange');
```

(assuming you have `devtools` installed; otherwise, install that first
using the `install.packages` function)
