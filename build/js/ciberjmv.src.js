
// This file is an automatically generated and should not be edited

'use strict';

const options = [{"name":"data","type":"Data"},{"name":"determinants","title":"(Sub-) determinants: ","type":"Variables","permitted":["numeric"],"suggested":["continuous"]},{"name":"targets","title":"Targets: ","type":"Variables","permitted":["numeric"],"suggested":["continuous"]},{"name":"binaryTargets","title":"Targets are binary variables","type":"Bool","default":false},{"name":"confLevelMeans","title":"Confidence of confidence intervals for means: ","type":"Number","default":99.99},{"name":"confLevelAssociations","title":"Confidence of confidence intervals for associations: ","type":"Number","default":95}];

const view = function() {
    
    this.handlers = { }

    View.extend({
        jus: "3.0",

        events: [

	]

    }).call(this);
}

view.layout = ui.extend({

    label: "Confidence Interval-Based Estimation of Relevance (CIBER)",
    jus: "3.0",
    type: "root",
    stage: 0, //0 - release, 1 - development, 2 - proposed
    controls: [
		{
			type: DefaultControls.VariableSupplier,
			typeName: 'VariableSupplier',
			persistentItems: false,
			stretchFactor: 1,
			controls: [
				{
					type: DefaultControls.TargetLayoutBox,
					typeName: 'TargetLayoutBox',
					label: "(Sub-) determinants",
					controls: [
						{
							type: DefaultControls.VariablesListBox,
							typeName: 'VariablesListBox',
							name: "determinants",
							isTarget: true
						}
					]
				},
				{
					type: DefaultControls.TargetLayoutBox,
					typeName: 'TargetLayoutBox',
					label: "Targets",
					controls: [
						{
							type: DefaultControls.VariablesListBox,
							typeName: 'VariablesListBox',
							name: "targets",
							isTarget: true
						}
					]
				}
			]
		},
		{
			type: DefaultControls.LayoutBox,
			typeName: 'LayoutBox',
			margin: "large",
			controls: [
				{
					type: DefaultControls.CheckBox,
					typeName: 'CheckBox',
					name: "binaryTargets"
				}
			]
		},
		{
			type: DefaultControls.LayoutBox,
			typeName: 'LayoutBox',
			margin: "large",
			controls: [
				{
					type: DefaultControls.TextBox,
					typeName: 'TextBox',
					name: "confLevelMeans",
					format: FormatDef.number,
					suffix: "%"
				}
			]
		},
		{
			type: DefaultControls.LayoutBox,
			typeName: 'LayoutBox',
			margin: "large",
			controls: [
				{
					type: DefaultControls.TextBox,
					typeName: 'TextBox',
					name: "confLevelAssociations",
					format: FormatDef.number,
					suffix: "%"
				}
			]
		}
	]
});

module.exports = { view : view, options: options };
